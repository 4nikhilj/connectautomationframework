package Test;

import in.co.eko.Actions.*;
import in.co.eko.ConnectUtil.TestUtility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by nikhil on 14/11/17.
 */


public class LoginConnectTest {

    WebDriver driver;
    HomePageAction hpa;
    SkipTourPageAction skipTourPageActionObj;
    SendCashAction sendCashActionObj;
    MobilePaymentAction mobilePaymentActionObj;

    @BeforeMethod
    public void initTest() throws InterruptedException
    {
        System.setProperty("webdriver.gecko.driver","/usr/local/bin/geckodriver");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
        driver.get("https://connectbeta.eko.co.in/");
        LoginAction lgn = new LoginAction();
        skipTourPageActionObj = lgn.loginWithGoogle(driver);
        hpa = skipTourPageActionObj.Skip_Esc(driver);
    }

//@BeforeTest
//    public void verifyLogin() throws InterruptedException {
//    LoginAction lgn = new LoginAction();
//    skipTourPageActionObj = lgn.loginWithGoogle(driver);
//    hpa = skipTourPageActionObj.Skip_Esc(driver);
//}


//@DataProvider
//    public Object[][] getData() throws IOException
//    {
//        return TestUtility.getTestData("Sheet1");
//    }
//
//@Test(dataProvider = "getData")
//        public void testMobileRecharge(String mobilenum)
//    {
//
//    }

@Test(priority=1, retryAnalyzer = Analyser.RetryAnalyser.class)
    public void testSendMoney() throws InterruptedException {

    sendCashActionObj = hpa.clickSendCashMenu(driver);
    sendCashActionObj.enterMobileNumberAndSubmit();
    System.out.println("send cash clicked");
    }

@Test(priority =2,invocationTimeOut = 1000L)
    public void testMobilePayment() throws InterruptedException
    {
        mobilePaymentActionObj = hpa.clickMobilePaymentMenu(driver);
        mobilePaymentActionObj.fillDataforMobilePaymentandSubmit();
        System.out.println("Mobile Payment clicked: Test");

    }

@AfterMethod
    public void tearDown()
    {
        System.out.println("After Test");
        driver.quit();
    }


}