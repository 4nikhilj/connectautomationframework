package Test;

import java.util.HashMap;

/**
 * Created by nikhil on 19/12/17.
 */
public class TestJava {

    public static void main(String[] args)
    {

        System.out.println(testString("Nikhil","nikhil nik nikhil nikhil"));
    }


    public static int testString(String text, String allText)
    {
        HashMap<String,Integer > hashMap= new HashMap<String, Integer>();
        String[] strArray= allText.split(" ");

        for (String i: strArray
                ) {
            if (hashMap.containsKey(i))
            {
                hashMap.put(i,hashMap.get(i)+1);
            }
            else
            {
                hashMap.put(i,1);
            }

        }
        System.out.println(hashMap.get(text));
        return hashMap.get(text);
    }

}
