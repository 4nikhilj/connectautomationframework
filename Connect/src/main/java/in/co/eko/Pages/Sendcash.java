package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by nikhil on 17/11/17.
 */

public class Sendcash {

    private WebDriver driver;

    public Sendcash(WebDriver driver)
    {
        this.driver = driver;
    }

    @FindBy(id = "inp0")
    public By mobilenum = new By.ById("inp0");

    @FindBy(id="btn_go")
    public By submitBtn = new By.ById("btn_go");

}
