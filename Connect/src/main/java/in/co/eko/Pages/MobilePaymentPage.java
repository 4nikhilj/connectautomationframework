package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.internal.FindsByCssSelector;
import org.openqa.selenium.support.FindBy;

/**
 * Created by nikhil on 14/12/17.
 */
public class MobilePaymentPage {

    @FindBy(css= "#qkselect > div.lbl")
    public By conType = new By.ByCssSelector("#qkselect > div.lbl");
    @FindBy(css= "#extitm2")
    public By mobileNumber = new By.ByCssSelector("#extitm2");
    @FindBy(css= "#inp5")
    public By amount = new By.ByCssSelector("#inp5");
    @FindBy(css= "#inp6")
    public By pin = new By.ByCssSelector("#inp6");
    @FindBy(css= "#btn_go")
    public By submit = new By.ByCssSelector("#btn_go");


}
