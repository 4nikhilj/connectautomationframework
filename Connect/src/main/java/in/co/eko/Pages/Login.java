package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.nio.file.WatchEvent;

/**
 * Created by nikhil on 14/11/17.
 */
public class Login {

    protected WebDriver driver;
    By loginIntoGoogle = new By.ById("usernameOrEmail");
    By password = new By.ById("password");
    By signIn = new By.ByClassName("login__form-action");

    public Login(WebDriver driver)
    {
        this.driver = driver;
    }

    public void fillUsernamePasswordAndClick()
    {
       System.out.println("Try to login");
        WebElement fillUserName = driver.findElement(loginIntoGoogle);
        fillUserName.sendKeys("4nikhilj@gmail.com");
        WebElement fillPassword = driver.findElement(password);
        fillPassword.sendKeys("9953895242a");
        WebElement clickButton = driver.findElement(signIn);
        clickButton.click();
    }

    public String getPageTitle(){
        String title = driver.getTitle();
        return title;
    }
}
