package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by nikhil on 14/11/17.
 */

public class ConnectLogin {

    private WebDriver driver;

    public ConnectLogin(WebDriver driver)
    {
        this.driver = driver;
     //   PageFactory.initElements(driver,this);
    }

    @FindBy(id = "google_signin")
    public  By loginButton = new By.ById("google_signin");

}
