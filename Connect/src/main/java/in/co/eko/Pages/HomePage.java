package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

/**
 * Created by nikhil on 12/12/17.
 */
public class HomePage {

    @FindBy(xpath = ".//*[@data-href ='/transaction/33']")
    public By sendMoneyMenu = new By.ByXPath(".//*[@data-href ='/transaction/33']");

    @FindBy(xpath = ".//*[@data-href ='/transaction/31']")
    public By utilityServiceMenu =new By.ByXPath(".//*[@data-href ='/transaction/31']");

    @FindBy(xpath = ".//*[@data-href ='/transaction/66']")
    public By creditCardMenu =new By.ByXPath(".//*[@data-href ='/transaction/66']");

    @FindBy(xpath = ".//*[@data-title ='Mobile']")
    public By mobileMenu = new By.ByXPath(".//*[@data-title ='Mobile']");
}
