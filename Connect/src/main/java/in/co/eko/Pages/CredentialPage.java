package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by nikhil on 14/11/17.
 */

public class CredentialPage {

    private WebDriver driver;

    public CredentialPage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath ="//input[@id='identifierId'][@aria-label='Email or phone']" )
    public  By userName = new By.ByXPath("//input[@id='identifierId'][@aria-label='Email or phone']");

    @FindBy(xpath="//span[@class='RveJvd snByac'][text()='Next']")
    public By nextBtn = new By.ByXPath("//span[@class='RveJvd snByac'][text()='Next']");

    @FindBy(name="password")
    public By password= new By.ByName("password");

    @FindBy(id="passwordNext")
    public By passwordNext= new By.ById("passwordNext");

public String getPageName()
{
  return driver.getTitle();
}

}
