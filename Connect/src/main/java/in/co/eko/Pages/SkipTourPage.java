package in.co.eko.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by nikhil on 13/12/17.
 */
public class SkipTourPage {
    private static WebElement element;

    public static WebElement Skip_Module(WebDriver driver) {
        element=driver.findElement(By.xpath("//paper-button[@id='skip']"));
        return element;
    }

//    public static WebElement Skip_Button(WebDriver driver) {
//        element=driver.findElement(By.id("overlay_back"));
//        return element;
//    }

    public By Skip_Button = new By.ById("overlay_back");

    public static WebElement Skip_B(WebDriver driver) {
        element=driver.findElement(By.xpath("//span[contains(@class,'btn')]"));
        return element;
    }
}
