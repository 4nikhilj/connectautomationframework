package in.co.eko.ConnectUtil;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by nikhil on 15/12/17.
 */
public class TestUtility {

    public static String path = "/home/nikhil/JKT/Connect/src/main/java" +
            "/in/co/eko/TestData/Mobilenumbers.xlsx";

    static HSSFWorkbook workbook;
    static HSSFSheet sheet;

    public static Object[][] getTestData(String sheetName) throws IOException
    {
        ArrayList arr;
        FileInputStream file = null;
        try {
            file = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        workbook = new HSSFWorkbook(file);
        sheet = workbook.getSheet(sheetName);

        Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
        for (int i = 0; i < sheet.getLastRowNum(); i++) {
            for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
                data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
                // System.out.println(data[i][k]);
            }
        }

       return data;
    }
}
