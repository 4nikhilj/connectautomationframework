package in.co.eko.Actions;

import in.co.eko.Pages.ConnectLogin;
import in.co.eko.Pages.CredentialPage;
import in.co.eko.Pages.HomePage;
import in.co.eko.Pages.SkipTourPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;


/**
 * Created by nikhil on 15/11/17.
 */
public class LoginAction {

    public static String parent;
    public static String child;

    ConnectLogin loginPageObject;
    CredentialPage credentialPageObject;
    public SkipTourPageAction loginWithGoogle(WebDriver driver) throws InterruptedException {
        System.out.println("Try to login into connect");
        Thread.sleep(5000);
        WebDriverWait wait = new WebDriverWait(driver, 10000);

        loginPageObject = PageFactory.initElements(driver, ConnectLogin.class);

        wait.until(ExpectedConditions.visibilityOfElementLocated(loginPageObject.loginButton));
        WebElement clickLoginWithGoogle = driver.findElement(loginPageObject.loginButton);

        parent = driver.getWindowHandle();
        clickLoginWithGoogle.click();
        Set<String> st = driver.getWindowHandles();
        Iterator<String> I1 = st.iterator();
        while (I1.hasNext()) {
            child = I1.next();
            if (!parent.equals(child)) {
                driver.switchTo().window(child);

                System.out.println("window switched");

                Thread.sleep(10000);
                credentialPageObject = PageFactory.initElements(driver,CredentialPage.class);
                wait.until(ExpectedConditions.visibilityOfElementLocated(credentialPageObject.userName));
                driver.findElement(credentialPageObject.userName).sendKeys("ekoninja2");

                wait.until(ExpectedConditions.visibilityOfElementLocated(credentialPageObject.nextBtn));
                driver.findElement(credentialPageObject.nextBtn).click();

                wait.until(ExpectedConditions.visibilityOfElementLocated(credentialPageObject.password));
                driver.findElement(credentialPageObject.password).sendKeys("overhill");

                wait.until(ExpectedConditions.visibilityOfElementLocated(credentialPageObject.passwordNext));
                driver.findElement(credentialPageObject.passwordNext).click();

                driver.switchTo().window(parent);
                System.out.print("switched back to Parent");

                Thread.sleep(10000);
            }
        }
        return new SkipTourPageAction(driver);
    }
}
