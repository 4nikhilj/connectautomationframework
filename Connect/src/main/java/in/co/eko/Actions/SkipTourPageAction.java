package in.co.eko.Actions;

import in.co.eko.Pages.HomePage;
import in.co.eko.Pages.SkipTourPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by nikhil on 13/12/17.
 */
public class SkipTourPageAction {

    WebDriver driver;
    protected WebDriverWait wait;
    SkipTourPage skiptourPageObj;
    public SkipTourPageAction(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
        skiptourPageObj = PageFactory.initElements(driver,SkipTourPage.class);
    }

    public HomePageAction Skip_Esc(WebDriver driver) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(skiptourPageObj.Skip_Button));
        WebElement Skip_Button = driver.findElement(skiptourPageObj.Skip_Button);
        Skip_Button.sendKeys(Keys.ESCAPE);
        System.out.print("Tour Escaped");
        return new HomePageAction(driver);
    }


}
