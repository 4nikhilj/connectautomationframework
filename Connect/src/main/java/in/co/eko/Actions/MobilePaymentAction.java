package in.co.eko.Actions;

import in.co.eko.Pages.MobilePaymentPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by nikhil on 14/12/17.
 */
public class MobilePaymentAction {

    WebDriver driver;
    WebDriverWait wait;
    MobilePaymentPage mobilePaymentObj;
    public MobilePaymentAction(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
        mobilePaymentObj = PageFactory.initElements(driver,MobilePaymentPage.class);
    }

    public void fillDataforMobilePaymentandSubmit()
    {
        wait.until(ExpectedConditions.visibilityOfElementLocated(mobilePaymentObj.conType));

        System.out.println("Inside Mobile payment fill data");
        WebElement conType = driver.findElement(mobilePaymentObj.conType);
        conType.click();

        WebElement mobileNumber = driver.findElement(mobilePaymentObj.mobileNumber);
        mobileNumber.sendKeys("9953895242");

        WebElement amount = driver.findElement(mobilePaymentObj.amount);
        amount.sendKeys("1000");

        WebElement pin = driver.findElement((mobilePaymentObj.pin));
        pin.sendKeys("0189");

        WebElement submit = driver.findElement(mobilePaymentObj.submit);
        submit.click();
    }
}
