package in.co.eko.Actions;

import in.co.eko.Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by nikhil on 14/12/17.
 */
public class HomePageAction {
    HomePage hp;
    WebDriver driver;
    WebDriverWait wait;

    public HomePageAction(WebDriver driver)
    {
    this.driver = driver;
    wait = new WebDriverWait(driver, 10000);
    hp = PageFactory.initElements(driver,HomePage.class );
    }

    public SendCashAction clickSendCashMenu(WebDriver driver)
    {
     wait.until(ExpectedConditions.visibilityOfElementLocated(hp.sendMoneyMenu));
     WebElement sendMoneyMenu = driver.findElement(hp.sendMoneyMenu);
     sendMoneyMenu.click();
     return new SendCashAction(driver);
    }

    public MobilePaymentAction clickMobilePaymentMenu (WebDriver driver)
    {
        System.out.println("Trying to click on Mobile");
       // this.driver = driver;
        wait.until(ExpectedConditions.visibilityOfElementLocated(hp.mobileMenu));
        WebElement mobileMenu = driver.findElement(hp.mobileMenu);
        mobileMenu.click();
        System.out.println("Mobile menu Clicked");
        return new MobilePaymentAction(driver);

    }
//    public QuickSendCashAction sendCash()
//    {
//TBD
//    }

}
