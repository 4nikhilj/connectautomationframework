package in.co.eko.Actions;

import in.co.eko.Pages.HomePage;
import in.co.eko.Pages.Sendcash;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by nikhil on 12/12/17.
 */
public class SendCashAction {
    Sendcash sendCashPageObj;
    WebDriverWait wait;
    WebDriver driver;

    public SendCashAction(WebDriver driver)
    {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10000);
        sendCashPageObj = PageFactory.initElements(driver,Sendcash.class);
    }
    public void enterMobileNumberAndSubmit(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(sendCashPageObj.mobilenum));
        WebElement mobilenum = driver.findElement(sendCashPageObj.mobilenum);
        mobilenum.sendKeys("9953895242");
        WebElement submitBtn = driver.findElement(sendCashPageObj.submitBtn);
        submitBtn.click();
    }
}
