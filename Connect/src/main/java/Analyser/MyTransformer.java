package Analyser;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Created by nikhil on 17/12/17.
 */
public class MyTransformer implements IAnnotationTransformer {

    public void transform(ITestAnnotation annotation, Class LoginConnectTest, Constructor testConstructor, Method testMethod)
    {
     annotation.setRetryAnalyzer(RetryAnalyser.class);

    }
}
