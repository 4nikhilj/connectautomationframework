package Analyser;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

/**
 * Created by nikhil on 17/12/17.
 */
public class RetryAnalyser implements IRetryAnalyzer {
    int counter = 0;
    int limit = 3;

    public boolean retry(ITestResult result)
    {
        if (counter< limit)
        {
            counter++;
            return true;
        }
        return false;
    }

}
